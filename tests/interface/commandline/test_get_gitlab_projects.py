from subprocess import Popen, PIPE


def test_get_gitlab_projects_is_available_from_command_line():
    p = Popen(["gllm", "get-gitlab-projects", "--help"], stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    assert "usage: gllm" in stdout.decode('utf-8')

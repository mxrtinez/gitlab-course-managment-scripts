import configparser


def load():
    config = configparser.ConfigParser()
    config.read('gllm.ini')
    return config

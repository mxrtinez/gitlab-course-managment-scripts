gllm - GitLab Learning Management
=================================

`gllm` (pronounced "gollum") is a command-line tool to manage students and projects in GitLab.

Requirements
------------

* Python 3.7
* Reads configuration from gllm.ini

## Configuration file

Copy gllm.ini.sample to gllm.ini and edit for your configuration.

Configuration variables:
* gitlabUsername - your GitLab username on the server
* gitlabUrl - the URL of your GitLab server
* gitlabToken - your private GitLab token

Create your private GitLab token on https://gitmilab.redclara.net/-/profile/personal_access_tokens


## Installation

> It's recomended to work in a [virtual environment](https://virtualenv.pypa.io/en/latest/index.html)

1. Clone repository and move to directory
```
git clone git@gitlab.com/mxrtinez/gitlab-course-managment-scripts.git
cd gitlab-course-managment-scripts
```
2. Build
```
python3.7 setup.py build
```
5. Install
```
python3.7 setup.py  install
```

## `get-gitlab-projects`
**Gets (clones) all GitLab projects that were forked from a particular project.**

This script was written for the following use-case:

* The user is an instructor using GitLab for students to submit assignments.
* The instructor created a GitLab project from which the students fork their own copy.
* The students have added the instructor to their project as at least a "Reporter" for the instructor to be able to clone the project, at least a "Developer" for the instructor to push changes to an unprotected  branch, or "Maintainer" for the instructor to push changset to a protected branch.

Call as:

`gllm get-gitlab-projects.py group-name project-name directory`

where

* *group-name* is the name of the GitLab group e.g. `cs-140-01-02-spring-2014`
* *project-name* is the name of the GitLab project name e.g. `Lab1`.
This is the part after the last / in the project URL. In the example:
`http://gitlab.com/myschool/cs-140-01-02-spring-2014/Lab1`
* *directory* is the path to the local directory where the projects should be cloned

## `remove-user-from-gitlab-projects`
**Remove the instructor from all GitLab projects forked from a specified group - in this case a course/semester group**

Call as:

`gllm remove-user-from-gitlab-projects.py group-name`

where

* *group-name* is the name of the GitLab group e.g. `cs-140-01-02-spring-2014`
